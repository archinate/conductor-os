# Development for Conductor has been discontinued... #
Greetings Conductor users,

Please note that I have decided to stop development and maintenance on this project. Feel free to use and 
reference this code if you find it valuable in your work.

Thank you for your interest - I hope the code can provide some value even though I will no longer be working 
on this project.

Best regards,

Nate Miller

CEO, [Proving Ground](http://provingground.io)

![ConductorLogo.jpg](ConductorLogo.jpg)

Conductor is an desktop app that connects your AEC tools to Trello's simple and powerful project management platform. Using Conductor, users are able to interact with Trello boards and cards from within their favorite AEC tools including Revit, Rhino, and Navisworks.

## Technical Information ##

* Desktop Windows apps written in C# with Visual Studio
* Plugins require Revit 2017, Revit 2018, Navisworks 2017 and Rhinoceros 5.0
* Dependencies include Newtonsoft Json.NET, Trello.NET, and RestSharp (builds provided)

## Getting Started ##

* Clone the repository
* Restore all NuGet packages
* Build the Revit, Rhino, or Navisworks project of your choosing

## GNU License ##
Copyright (C) 2018 Proving Ground LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/