﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace ProvingGround.ConductorRevit.Utilities
{
    public static class clsGlobals
    {
        public static DockablePaneId MainDockablePaneId = new DockablePaneId(new Guid("{ddfd051c-2dcf-40df-afa8-c51f15636801}"));
        public static string MainPanelName()
        {
            return "Conductor v" + Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}
