﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using System.Data;
using System.IO;
using System.Xml;

using ProvingGround.ConductorInterface.Classes;

using ProvingGround.ConductorRevit.Classes;
using ProvingGround.ConductorRevit.Entry;

using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Events;

using TrelloNet;

using Visibility = System.Windows.Visibility;


namespace ProvingGround.ConductorRevit.UI
{
    /// <summary>
    /// Interaction logic for form_MainDock.xaml
    /// </summary>
    public partial class form_MainDock : Page, IDockablePaneProvider
    {
        private ExtCmdModeless _handler = new ExtCmdModeless();

        public form_MainDock()
        {
            Process m_appProcess = Process.GetCurrentProcess();
            clsCurrentApp.Handle = m_appProcess.MainWindowHandle;

            InitializeComponent();
        }

        public void SetupDockablePane(DockablePaneProviderData data)
        {
            data.FrameworkElement = this;
            DockablePaneProviderData d = new DockablePaneProviderData();
            data.InitialState = new DockablePaneState();
            data.InitialState.DockPosition = DockPosition.Right;
        }

        /// <summary>
        /// Size Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetScaling();
        }

        /// <summary>
        /// Hot fix for windows 8 video DPI scaling
        /// </summary>
        private void SetScaling()
        {
            try
            {

                //Matrix m = PresentationSource.FromVisual(this).CompositionTarget.TransformToDevice;
                //double dx = m.M11;
                //double dy = m.M22;
                //GridMain.Margin = new Thickness((this.ActualWidth / dx) - this.ActualWidth, (this.ActualHeight / dy) - this.ActualHeight, 0, 0);
                //GridMain.Width = this.ActualWidth / dx;
                //GridMain.Height = this.ActualHeight / dy;

            }
            catch { }
        }

        private void TrelloDock_Loaded(object sender, RoutedEventArgs e)
        {

        }

    }
}
