﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Threading;

using TrelloNet;

namespace ProvingGround.ConductorRevit.Classes
{
    /// <summary>
    /// Helper Class for Trello
    /// </summary>
    public class clsTrelloUtils
    {
        private ITrello _trello;
        private string _trellokey = "eacbb7917db670e7f9dc1ab01d0d37ef";
        //private string _secret = "fb5fc5f852a7330a40c34e31998c0133d391eecd0f98cbd9987aab74323166fb";

        #region Public Properties
        /// <summary>
        /// My Membership
        /// </summary>
        public Member MyMembership
        {
            get
            {
                return _trello.Members.Me();
            }
        }

        /// <summary>
        /// Authenticated Member Name
        /// </summary>
        public string AuthMemberName
        {
            get
            {
                Member m_me = _trello.Members.Me();
                string m_name = m_me.FullName;
                return m_name;
            }
        }

        /// <summary>
        /// List of all my Organizations
        /// </summary>
        public List<Organization> AllOrganizations
        {
            get
            {
                IEnumerable<Organization> m_orgs = _trello.Organizations.ForMe(OrganizationFilter.All);
                return m_orgs.ToList();
            }
        }

        /// <summary>
        /// List of all of my Boards
        /// </summary>
        public List<Board> OpenBoards
        {
            get
            {
                IEnumerable<Board> m_boards = _trello.Boards.ForMe(BoardFilter.Open);
                return m_boards.ToList();
            }
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public clsTrelloUtils()
        {
            //widen scope
            SetupTrello();
        }

        #region Public Members - Access
        /// <summary>
        /// Get Members on a Board
        /// </summary>
        /// <param name="board">Trello Board</param>
        /// <returns>List of Members</returns>
        public List<Member> GetBoardMembers(Board board)
        {
            try
            {
               IEnumerable<Member> m_members = _trello.Members.ForBoard(board, MemberFilter.All);
               return m_members.ToList();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a list of Trello boards by Organization
        /// </summary>
        /// <param name="org">A Trello Organization</param>
        /// <returns></returns>
        public List<Board> GetTrelloBoardsbyOrganization(Organization org)
        {
            try
            {
                IEnumerable<Board> m_boards = _trello.Boards.ForOrganization(org, BoardFilter.Open);
                return m_boards.ToList();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a list of Trello Lists on a Board
        /// </summary>
        /// <param name="trelloboard">Trello Board</param>
        /// <returns>List of Lists</returns>
        public List<List> GetTrelloLists(Board trelloboard)
        {
            try
            {
                IEnumerable<List> m_lists = _trello.Lists.ForBoard(trelloboard, ListFilter.Open);
                return m_lists.ToList();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a list of Trello Cards from a List
        /// </summary>
        /// <param name="trellolist">Trello List</param>
        /// <returns>List of Cards</returns>
        public List<Card> GetTrelloCards(List trellolist)
        {
            try
            {
                IEnumerable<Card> m_cards = _trello.Cards.ForList(trellolist, CardFilter.Open);
                return m_cards.ToList();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get Trello checklists
        /// </summary>
        /// <param name="card">A Trello card</param>
        /// <returns>A list of Checklists</returns>
        public List<Card.Checklist> GetTrelloCheckLists(Card card)
        {
            try
            {
                return card.Checklists;
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a list of members on a card
        /// </summary>
        /// <param name="card">Trello Card</param>
        /// <returns>List of Members</returns>
        public List<string> GetTrelloMembersOnCard(Card card)
        {
            try
            {
                List<string> m_ids = card.IdMembers;
                List<string> m_names = new List<string>();
                foreach (string m in m_ids)
                {
                    Member mem = _trello.Members.WithId(m);
                    m_names.Add(mem.FullName);
                }

                return m_names;
            }
            catch { return null; }
        }

        /// <summary>
        /// Get Trello Checklist items
        /// </summary>
        /// <param name="checklist"></param>
        /// <returns>A list of Checklist items</returns>
        public List<Card.CheckItem> GetCheckListItems (Card.Checklist checklist)
        {
            try
            {
                List<Card.CheckItem> m_checkitems = checklist.CheckItems;

                return m_checkitems;
            }
            catch { return null; }
        }
        #endregion

        #region Public Members - Authorization
        /// <summary>
        /// Authorize the Application
        /// </summary>
        public Uri AuthorizeURL()
        {
            try
            {
                Uri m_url = _trello.GetAuthorizationUrl("Trello for Revit", Scope.ReadWrite);
                return m_url;
            }
            catch { return null; }
        }

        /// <summary>
        /// Authorize the App
        /// </summary>
        /// <param name="token">The token from the authorization website</param>
        /// <returns>True if successful</returns>
        public bool Authorize(string token)
        {
            try
            {
                _trello.Authorize(token);
                return true;
            }
            catch { return false; }
        }
        #endregion

        #region Public Members - Create or Update
        /// <summary>
        /// Create a new Trello Card on a List
        /// </summary>
        /// <param name="trellolist">Trello List</param>
        /// <param name="name">Name of the card</param>
        /// <param name="description">Card Description</param>
        /// <param name="member">Member assigned to the card.</param>
        /// <param name="due">Due date for the card.</param>
        public Card CreateCard(List trellolist, string name, string description, Member member, DateTime due, List<CheckBox> checklistitems)
        {
            try
            {
                // Create card with data
                Board m_board = _trello.Boards.WithId(trellolist.IdBoard);
                Card m_newcard = _trello.Cards.Add(new NewCard(name, trellolist));
                _trello.Cards.AddMember(m_newcard, member);
                _trello.Cards.ChangeDueDate(m_newcard, due);
                _trello.Cards.ChangeDescription(m_newcard, description);

                // Add Checklist
                Checklist m_checklist = _trello.Checklists.Add("My Checklist", m_newcard);
                foreach (CheckBox cb in checklistitems)
                {
                    _trello.Checklists.AddCheckItem(m_checklist, cb.Content.ToString());
                }
                _trello.Cards.AddChecklist(m_newcard, m_checklist);

                return m_newcard;
            }
            catch { return null; }
        }

        /// <summary>
        /// Update a Card with new info
        /// </summary>
        /// <param name="checlistitems">List of Checklist items</param>
        /// <param name="due">Due date</param>
        /// <returns>Updated card</returns>
        public Card UpdateCard(Card updatedcard, Card.Checklist checklist, List<CheckBox> checklistitems, DateTime due)
        {
            try
            {
                _trello.Cards.ChangeDueDate(updatedcard, due);
                foreach (CheckBox cb in checklistitems)
                {
                    Card.CheckItem m_ci = (Card.CheckItem)cb.Tag;
                    ICardId m_cardid = updatedcard;
                    IChecklistId m_checklistid = checklist;
                    ICheckItemId m_checkitemid = m_ci;

                    _trello.Async.Cards.ChangeCheckItemState(m_cardid, m_checklistid, m_checkitemid, cb.IsChecked.Value);
                }
                return updatedcard;
            }
            catch { return null; }
        }
        #endregion

        #region Private Members - Setup
        /// <summary>
        /// Setup Trello
        /// </summary>
        private void SetupTrello()
        {
            try
            {
                _trello = new Trello(_trellokey);
            }
            catch { }
        }
        #endregion
    }
}
