﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;

using ProvingGround.ConductorRevit.Utilities;
using ProvingGround.ConductorRevit.Classes;
#endregion

namespace ProvingGround.ConductorRevit.Entry
{
    [Transaction(TransactionMode.Manual)]
    public class CmdModeless : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,
          ElementSet elements)
        {

                // Settings Class
                AppMain.Settings = new clsSettings(commandData);

                DockablePaneId m_dpID = clsGlobals.MainDockablePaneId;
                DockablePane m_dp = commandData.Application.GetDockablePane(m_dpID);
                m_dp.Show();

                return Result.Succeeded;
        }
    }
}