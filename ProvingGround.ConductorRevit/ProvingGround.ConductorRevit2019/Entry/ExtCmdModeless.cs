﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace ProvingGround.ConductorRevit.Entry
{
    public enum EnumCommandType : int
    {
        NoCommand,
        ExampleCommand
    }

    class ExtCmdModeless : IExternalEventHandler
    {
        /// <summary>
        /// Model Updates for Modeless Dialog
        /// </summary>
        /// <param name="app"></param>
        public void Execute(UIApplication app)
        {
            switch (AppMain.Settings.CommandType)
            {
                case EnumCommandType.ExampleCommand:
                    ExampleCommand(app);
                    break;
            }
        }

        private void ExampleCommand(UIApplication app)
        {
            try
            {
                using (Transaction t = new Transaction(app.ActiveUIDocument.Document, "Do Something."))
                {
                    // Do somthing
                    t.Start();


                    t.Commit();
                }
            }
            catch { }
        }

        /// <summary>
        /// External event anme
        /// </summary>
        /// <returns></returns>
        public string GetName() { return "ExtCmdModeless"; }

    }
}
