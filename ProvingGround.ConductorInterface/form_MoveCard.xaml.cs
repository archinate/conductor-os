﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using System.Data;
using System.IO;
using System.Xml;

using TrelloNet;
using ProvingGround.ConductorInterface.Classes;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class form_MoveCard : Window
    {

        #region private members

        private clsTrelloUtils _trelloutils;
        private List<TrelloNet.List> _lists;
        private Card _card;
        private int _startIndex;

        #endregion

        public form_MoveCard(clsTrelloUtils trelloutils, List<TrelloNet.List> lists, Card card, int startIndex)
        {
            InitializeComponent();

            _trelloutils = trelloutils;
            _lists = lists;
            _card = card;
            _startIndex = startIndex;

            DoSetup();
        }

        #region Private Members - Form Actions

        private void DoSetup()
        {

            Label_CardName.Content = _card.Name;

            for (int i = 0; i < _lists.Count; i++)
            {
                ListBox_Lists.Items.Add(_lists[i].Name);
            }

            ListBox_Lists.SelectedIndex = _startIndex;

        }

        #endregion

        #region Form Events

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_MoveCard_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _trelloutils.Movecard(_lists[ListBox_Lists.SelectedIndex], _card);
                MessageBox.Show("Your card was updated and moved.", "Card Updated", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch { MessageBox.Show("There was a problem updating and moving the card.", "Move Failed.", MessageBoxButton.OK, MessageBoxImage.Error); }
            this.Close();
        }

        #endregion

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
