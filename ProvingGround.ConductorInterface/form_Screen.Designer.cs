﻿namespace ProvingGround.ConductorInterface
{
    partial class form_Screen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_Capture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Capture)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox_Capture.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_Capture.Name = "pictureBox1";
            this.pictureBox_Capture.Size = new System.Drawing.Size(100, 50);
            this.pictureBox_Capture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_Capture.TabIndex = 0;
            this.pictureBox_Capture.TabStop = false;
            this.pictureBox_Capture.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Capture_MouseDown);
            this.pictureBox_Capture.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Capture_MouseMove);
            // 
            // form_Screen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(279, 211);
            this.Controls.Add(this.pictureBox_Capture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "form_Screen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "form_Screen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.OnActivated);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Capture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_Capture;
    }
}