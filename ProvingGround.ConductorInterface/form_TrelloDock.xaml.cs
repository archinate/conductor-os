﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using System.Windows.Interop;

using System.Data;
using System.IO;
using System.Xml;

using TrelloNet;
using ProvingGround.ConductorInterface.Classes;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for SampleCsWpfPanelUserControl.xaml
    /// </summary>
    public partial class form_TrelloDock : UserControl
    {
        private clsTrelloUtils _trelloutils;
        private string _token;

        private string _configpath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Proving Ground", "Conductor", "ProvingGround.Trello.config");
        private string _boardpath;
        private string _cardpath;

        private Board _selectedboard;
        private TrelloNet.List _selectedlist;
        private TrelloNet.List _changelist;
        private TrelloNet.Card _selectedcard;
        private TrelloNet.Card.Checklist _selectedchecklist;

        #region Public Members

        // create the binding variable

        public Card SelectedCard
        {
            get { return _selectedcard; }
        }

        public bool IsCardSelected
        {
            get { return Button_UpdateCard.IsEnabled; }
        }

        public clsTrelloUtils TrelloUtils
        {
            get { return _trelloutils; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// The main trello dock
        /// </summary>
        public form_TrelloDock()
        {
            InitializeComponent();

            // for binding the Application name
            Binding _AppName = new Binding("ApplicationName");
            _AppName.Source = this;

            //Setup trello
            _trelloutils = new clsTrelloUtils();

            //Get config info
            bool m_info = GetConfigInfo();

            if (m_info == false)
            {
                Expander_UserSettings.IsExpanded = true;
            }
            else
            {
                _trelloutils.Authorize(_token);
                SetupForm();
            }
        }

        #endregion

        #region Private Members - Form Actions
        private void SetupForm()
        {
            try
            {
                // Auth Name
                string name = _trelloutils.AuthMemberName;
                if (name != "")
                {
                    Label_AuthUser.Content = name;

                    // Populate Organizations
                    ComboBoxItem m_cbxorgall = new ComboBoxItem();
                    m_cbxorgall.Content = "All Organizations";
                    ComboBox_Organization.Items.Add(m_cbxorgall);
                    foreach (Organization o in _trelloutils.AllOrganizations)
                    {
                        ComboBoxItem m_cbxitem = new ComboBoxItem();
                        m_cbxitem.Content = o.DisplayName;
                        m_cbxitem.Tag = o;
                        ComboBox_Organization.Items.Add(m_cbxitem);
                    }
                    ComboBox_Organization.SelectedIndex = 0;

                    // Populate Boards
                    ComboBox_Boards.Items.Clear();
                    foreach (Board b in _trelloutils.OpenBoards)
                    {
                        ComboBoxItem m_cbxitem = new ComboBoxItem();
                        m_cbxitem.Content = b.Name;
                        m_cbxitem.Tag = b;
                        ComboBox_Boards.Items.Add(m_cbxitem);
                    }

                    ComboBox_Lists.IsEnabled = false;
                    ComboBox_Cards.IsEnabled = false;
                    ComboBox_Members.IsEnabled = false;
                    ComboBox_Checklists.IsEnabled = false;
                    Button_CreateCard.IsEnabled = false;
                    Button_UpdateCard.IsEnabled = false;
                    ComboBox_ChangeCardList.IsEnabled = false;
                    Button_ScreenShot.IsEnabled = false;
                    Button_AddMembers.IsEnabled = false;
                    Button_AddLabels.IsEnabled = false;

                    //Button_MoveCard.IsEnabled = false;

                    ClearCardInfo();
                    EnableCardUI(false);

                }
                else { MessageBox.Show("Conductor could not authenticate the user.", "No Authentication", MessageBoxButton.OK, MessageBoxImage.Error); }
            }
            catch { }
        }

        /// <summary>
        /// Hot fix for windows 8 video DPI scaling
        /// </summary>
        private void SetScaling()
        {
            try
            {
                Matrix m = PresentationSource.FromVisual(this).CompositionTarget.TransformToDevice;
                double dx = m.M11;
                double dy = m.M22;
                GridMain.Width = this.Width / dx;
                GridMain.Height = this.Height / dy;
            }
            catch { }
        }

        /// <summary>
        /// Get Token Configuration Info
        /// </summary>
        /// <returns>True if config info available</returns>
        private bool GetConfigInfo()
        {
            try
            {
                if (System.IO.File.Exists(_configpath) == true)
                {
                    StreamReader m_sr = new StreamReader(_configpath);
                    string m_xml = m_sr.ReadToEnd();

                    if (m_xml != "")
                    {
                        XmlDocument m_xmldoc = new XmlDocument();
                        m_xmldoc.LoadXml(m_xml);

                        XmlNodeList m_nodelist = m_xmldoc.GetElementsByTagName("token");
                        List<string> m_innertext = new List<string>();
                        foreach (XmlNode x in m_nodelist)
                        {
                            m_innertext.Add(x.InnerText);
                        }


                        _token = m_innertext[0];
                        TextBox_Token.Text = m_innertext[0];

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch { return false; }
        }

        /// <summary>
        /// Save config info
        /// </summary>
        private void SetConfigInfo()
        {
            try
            {
                DataSet m_ds = new DataSet("TrelloConfig");
                DataTable m_dt = new DataTable("Authorization");
                m_dt.Columns.Add("token");
                DataRow m_r = m_dt.NewRow();
                m_r["token"] = TextBox_Token.Text;
                m_dt.Rows.Add(m_r);
                m_ds.Tables.Add(m_dt);

                XmlDocument m_xmldoc = new XmlDocument();
                m_xmldoc.LoadXml(m_ds.GetXml());

                m_xmldoc.Save(_configpath);
            }
            catch { }
        }
        #endregion

        #region Form Events

        /// <summary>
        /// Take screenshot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_ScreenShot_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime m_timeStamp = DateTime.Now;
                string fileName = m_timeStamp.ToString().Replace("/", "-").Replace(":", "-") + ".jpeg";
                string tempDir = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Proving Ground", "Conductor", "temp");
                if (Directory.Exists(tempDir) == false)
                {
                    Directory.CreateDirectory(tempDir);
                }

                string path = System.IO.Path.Combine(tempDir, fileName);
                form_Screen m_screen = new form_Screen(path, _trelloutils, _selectedcard);
                m_screen.Show();
            }
            catch { }

        }

        /// <summary>
        /// Hyperlink
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            try
            {
                Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));

            }
            catch { }
            e.Handled = true;

        }

        /// <summary>
        /// Button replacements for navigtion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_RequestNavigate(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(new ProcessStartInfo("www.trello.com"));

            }
            catch { }
            e.Handled = true;

        }

        /// <summary>
        /// Visit Authorization
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_RequestAuthNav(object sender, RoutedEventArgs e)
        {
            try
            {
                // visit authorization url
                Process.Start(new ProcessStartInfo(_trelloutils.AuthorizeURL().AbsoluteUri));


            }
            catch { }
            e.Handled = true;

        }

        /// <summary>
        /// Size Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetScaling();
        }

        /// <summary>
        /// Visit Authorization
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_RequestAuthNav(object sender, RequestNavigateEventArgs e)
        {
            try
            {
                // visit authorization url
                Process.Start(new ProcessStartInfo(_trelloutils.AuthorizeURL().AbsoluteUri));

            }
            catch { MessageBox.Show("Conductor could not navigate to the autorization URL.", "No Authorization.", MessageBoxButton.OK, MessageBoxImage.Error); }
            e.Handled = true;
        }

        /// <summary>
        /// Visit Card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_VisitCardNav(object sender, RequestNavigateEventArgs e)
        {
            try
            {
                // visit authorization url
                Process.Start(new ProcessStartInfo(_cardpath));

            }
            catch { MessageBox.Show("No card appears to be selected. Please select a card and try again.", "No Card Selected", MessageBoxButton.OK, MessageBoxImage.Error); }
            e.Handled = true;
        }

        /// <summary>
        /// Visit Board
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_VisitBoardNav(object sender, RequestNavigateEventArgs e)
        {
            try
            {
                // visit authorization url
                Process.Start(new ProcessStartInfo(_boardpath));

            }
            catch { MessageBox.Show("No board appears to be selected. Please select a board and try again.", "No Board Selected", MessageBoxButton.OK, MessageBoxImage.Error); }
            e.Handled = true;
        }

        /// <summary>
        /// Authorize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Auth_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Authorize with the token
                _token = TextBox_Token.Text;
                //Save configuration info
                SetConfigInfo();

                _trelloutils.Authorize(TextBox_Token.Text);

                // Setup form after authorization
                SetupForm();
            }
            catch { MessageBox.Show("Conductor could not authorize your board. Please check your token and try again.", "No Authorization", MessageBoxButton.OK, MessageBoxImage.Error); }
        }

        /// <summary>
        /// Create New Card Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_CreateCard_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                form_AddCard m_cardform = new form_AddCard(_trelloutils, _selectedboard, _selectedlist);

                WindowInteropHelper m_ownerHelper = new WindowInteropHelper(m_cardform);
                m_ownerHelper.Owner = clsCurrentApp.Handle;

                m_cardform.ShowDialog();

                _selectedcard = m_cardform.NewCard;
                RefreshCards();
                SelectNewCard();
            }
            catch { }
            e.Handled = true;
        }

        /// <summary>
        /// Update Card in Trello
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_UpdateCard_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime? m_date = null;
                try
                {
                    m_date = DatePicker_Due.SelectedDate.Value;
                }
                catch { }

                List<CheckBox> m_checks = new List<CheckBox>();
                foreach (CheckBox cb in ListBox_CheckList.Items)
                {
                    m_checks.Add(cb);
                }

                _trelloutils.UpdateCard(_selectedcard, _selectedchecklist, m_checks, m_date);

                if (ComboBox_Lists.SelectedIndex != ComboBox_ChangeCardList.SelectedIndex)
                {
                    _trelloutils.Movecard(_changelist, _selectedcard);
                }

                MessageBox.Show("Your card was updated.", "Card Updated", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a problem updating the card: " + ex.Message, "Update Failed.", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Opens dialog for moving card to new list in board
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void Button_MoveCard_Click(object sender, RoutedEventArgs e)
        //{
        //    if (ComboBox_Cards.SelectedIndex > -1 && ComboBox_Lists.SelectedIndex > -1)
        //    {
        //        List<TrelloNet.List> m_lists = _trelloutils.GetTrelloLists(_selectedboard);
        //        form_MoveCard m_moveCard = new form_MoveCard(_trelloutils, _trelloutils.GetTrelloLists(_selectedboard), _selectedcard, ComboBox_Lists.SelectedIndex);
        //        var location = Button_MoveCard.PointToScreen(new Point(0, 0));
        //        m_moveCard.Left = location.X;
        //        m_moveCard.Top = location.Y - m_moveCard.Height;
        //        m_moveCard.ShowDialog();
        //    }
        //}

        /// <summary>
        /// Add Comment to Card Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddComment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ComboBox_Cards.SelectedIndex > -1 && ComboBox_Lists.SelectedIndex > -1)
                {
                    form_AddComment m_addComment = new form_AddComment(_trelloutils, _selectedcard, this);
                    var location = Button_AddComment.PointToScreen(new Point(0, 0));
                    m_addComment.Left = location.X;
                    m_addComment.Top = location.Y - m_addComment.Height;

                    WindowInteropHelper m_ownerHelper = new WindowInteropHelper(m_addComment);
                    m_ownerHelper.Owner = clsCurrentApp.Handle;

                    m_addComment.ShowDialog();
                }
            }
            catch { }

        }

        /// <summary>
        /// Add Checklist to Card Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddChecklist_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ComboBox_Cards.SelectedIndex > -1 && ComboBox_Lists.SelectedIndex > -1)
                {
                    form_AddChecklist m_addChecklist = new form_AddChecklist(_trelloutils, _selectedcard, this);
                    var location = Label_SelectChecklist.PointToScreen(new Point(0, 0));
                    m_addChecklist.Left = location.X;
                    m_addChecklist.Top = location.Y - m_addChecklist.Height;

                    WindowInteropHelper m_ownerHelper = new WindowInteropHelper(m_addChecklist);
                    m_ownerHelper.Owner = clsCurrentApp.Handle;

                    m_addChecklist.ShowDialog();

                    RefreshCards();
                    SelectNewCard();
                }
            }
            catch { }

        }

        /// <summary>
        /// Add members to card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddLabels_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                form_AddLabel m_addLabels = new form_AddLabel(_trelloutils, _selectedboard, _selectedcard, this);
                var location = Label_Members.PointToScreen(new Point(0, 0));
                m_addLabels.Left = location.X;
                m_addLabels.Top = location.Y - m_addLabels.Height;

                WindowInteropHelper m_ownerHelper = new WindowInteropHelper(m_addLabels);
                m_ownerHelper.Owner = clsCurrentApp.Handle;

                m_addLabels.ShowDialog();

                RefreshCards();
                SelectNewCard();
            }
            catch { }

        }

        /// <summary>
        /// Add members to card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddMembers_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                form_AddMembers m_addMembers = new form_AddMembers(_trelloutils, _selectedboard, _selectedcard, this);
                var location = Label_Members.PointToScreen(new Point(0, 0));
                m_addMembers.Left = location.X;
                m_addMembers.Top = location.Y - m_addMembers.Height;

                WindowInteropHelper m_ownerHelper = new WindowInteropHelper(m_addMembers);
                m_ownerHelper.Owner = clsCurrentApp.Handle;

                m_addMembers.ShowDialog();

                RefreshCards();
                SelectNewCard();
            }
            catch { }

        }

        /// <summary>
        /// Upload new attachment to selected card Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddAttachment_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog m_uploadDialog = new System.Windows.Forms.OpenFileDialog();

            if (m_uploadDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    _trelloutils.AddAttachment(_selectedcard, m_uploadDialog.FileName);
                    System.Windows.Forms.MessageBox.Show("Attachment uploaded to " + _selectedcard.Name);
                    StackPanel_Attachments.Children.Add(new uc_Attachment(System.IO.Path.GetFileName(m_uploadDialog.FileName), "temp", false));
                }
                catch (Exception)
                {
                    System.Windows.Forms.MessageBox.Show("Unable to upload attachment.");
                }
            }
            else
            {
                return;
            }

        }

        /// <summary>
        /// Trello Token Get Focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_Token_GetFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Token.Text == "Paste token here...")
            {
                TextBox_Token.Text = "";
            }
        }

        /// <summary>
        /// Trello Token Lost Focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_Token_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Token.Text == "")
            {
                TextBox_Token.Text = "Paste token here...";
            }
        }
        #endregion

        #region Events - ComboBox
        /// <summary>
        /// Organization Selection Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_Organization_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ComboBox_Organization.SelectedIndex > -1)
                {
                    Button_CheckNewActions.IsEnabled = false;
                    if (ComboBox_Organization.SelectedIndex == 0)
                    {
                        ComboBox_Boards.Items.Clear();
                        // Populate Boards
                        foreach (Board b in _trelloutils.OpenBoards)
                        {
                            ComboBoxItem m_cbxitem = new ComboBoxItem();
                            m_cbxitem.Content = b.Name;
                            m_cbxitem.Tag = b;
                            ComboBox_Boards.Items.Add(m_cbxitem);
                        }
                    }
                    else
                    {
                        ComboBox_Boards.Items.Clear();
                        ComboBoxItem m_cbxitem = (ComboBoxItem)ComboBox_Organization.SelectedItem;
                        Organization m_organization = (Organization)m_cbxitem.Tag;
                        List<Board> m_boards = _trelloutils.GetTrelloBoardsbyOrganization(m_organization);

                        foreach (Board b in m_boards)
                        {
                            ComboBoxItem m_cbx = new ComboBoxItem();
                            m_cbx.Content = b.Name;
                            m_cbx.Tag = b;
                            ComboBox_Boards.Items.Add(m_cbx);
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Board Selection Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_Boards_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ComboBox_Boards.SelectedIndex > -1)
                {
                    ComboBox_Lists.IsEnabled = true;
                    ComboBox_Lists.Items.Clear();
                    ComboBoxItem m_cbxitem = (ComboBoxItem)ComboBox_Boards.SelectedItem;
                    Board m_board = (Board)m_cbxitem.Tag;
                    List<TrelloNet.List> m_lists = _trelloutils.GetTrelloLists(m_board);

                    foreach (TrelloNet.List l in m_lists)
                    {
                        ComboBoxItem m_cbx = new ComboBoxItem();
                        m_cbx.Content = l.Name;
                        m_cbx.Tag = l;
                        ComboBox_Lists.Items.Add(m_cbx);
                    }

                    _boardpath = m_board.Url;
                    _selectedboard = m_board;

                    Button_CheckNewActions.IsEnabled = true;
                }


                ComboBox_Cards.Items.Clear();
                ComboBox_Cards.IsEnabled = false;
                Button_CreateCard.IsEnabled = false;

                // clear card UI and disable UI
                ClearCardInfo();
                EnableCardUI(false);

            }
            catch { }
        }

        /// <summary>
        /// Card Selection Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_Lists_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ComboBox_Lists.SelectedIndex > -1)
                {
                    ComboBox_Cards.IsEnabled = true;
                    Button_CreateCard.IsEnabled = true;
                    ComboBox_Cards.Items.Clear();

                    ComboBoxItem m_cbxitem = (ComboBoxItem)ComboBox_Lists.SelectedItem;
                    TrelloNet.List m_list = (TrelloNet.List)m_cbxitem.Tag;
                    List<Card> m_cards = _trelloutils.GetTrelloCards(m_list);

                    foreach (Card c in m_cards)
                    {
                        ComboBoxItem m_cbx = new ComboBoxItem();
                        m_cbx.Content = c.Name;
                        m_cbx.Tag = c;
                        ComboBox_Cards.Items.Add(m_cbx);
                    }
                    _selectedlist = m_list;
                }

                // clear card UI and disable UI
                ClearCardInfo();
                EnableCardUI(false);
            }
            catch { }
        }

        /// <summary>
        /// Card Selection Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_Cards_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ComboBox_Cards.SelectedIndex > -1)
                {
                    ClearCardInfo();
                    EnableCardUI(true);

                    GetChangeLists();

                    ComboBoxItem m_cbxchecklist = (ComboBoxItem)ComboBox_Cards.SelectedItem;

                    Card m_card = (Card)m_cbxchecklist.Tag;

                    List<Card.Checklist> m_checklists = _trelloutils.GetTrelloCheckLists(m_card);
                    List<string> m_members = _trelloutils.GetTrelloMembersOnCard(m_card);

                    Label_CardName.Text = m_card.Name;

                    if (m_card.Due != null)
                    {
                        DatePicker_Due.SelectedDate = m_card.Due.Value;
                        DatePicker_Due.DisplayDate = m_card.Due.Value;
                    }

                    _cardpath = m_card.Url;
                    _selectedcard = m_card;

                    // Select appropriate list
                    ComboBox_ChangeCardList.SelectedIndex = ComboBox_Lists.SelectedIndex;

                    // add labels
                    for (int i = 0; i < m_card.Labels.Count; i++)
                    {
                        WrapPanel_Labels.Children.Add(new uc_Label(m_card.Labels[i].Name, m_card.Labels[i].Color));
                    }

                    // add comments
                    List<CommentCardAction> m_comments = _trelloutils.GetCardComments(m_card);
                    for (int i = 0; i < m_comments.Count; i++)
                    {
                        string m_memberName = m_comments[i].MemberCreator.FullName;
                        string m_dateTime = m_comments[i].Date.ToLocalTime().ToString("dd-MMM h:mm tt");
                        string m_comment = m_comments[i].Data.Text;
                        StackPanel_Comments.Children.Add(new uc_Comment(m_memberName, m_dateTime, m_comment));
                    }

                    // add attachments
                    foreach (Card.Attachment m_attachment in _selectedcard.Attachments)
                    {
                        string m_fileName = m_attachment.Name;
                        string m_url = m_attachment.Url;
                        StackPanel_Attachments.Children.Add(new uc_Attachment(m_fileName, m_url, true));
                    }

                    // populate members
                    foreach (string m in m_members)
                    {
                        ComboBoxItem m_cbx = new ComboBoxItem();
                        m_cbx.Content = m;
                        ComboBox_Members.Items.Add(m_cbx);
                    }
                    if (ComboBox_Members.Items.Count > 0)
                    {
                        ComboBox_Members.SelectedIndex = 0;
                    }

                    // populate checklists
                    foreach (Card.Checklist c in m_checklists)
                    {
                        ComboBoxItem m_cbx = new ComboBoxItem();
                        m_cbx.Content = c.Name;
                        m_cbx.Tag = c;
                        ComboBox_Checklists.Items.Add(m_cbx);
                    }
                    if (ComboBox_Checklists.Items.Count > 0)
                    {
                        ComboBox_Checklists.SelectedIndex = 0;
                    }

                    // populate checklist items
                    if (ComboBox_Checklists.SelectedIndex > -1)
                    {
                        ListBox_CheckList.Items.Clear();

                        ComboBoxItem m_cbxchecks = (ComboBoxItem)ComboBox_Checklists.SelectedItem;
                        Card.Checklist m_checklist = (Card.Checklist)m_cbxchecks.Tag;
                        List<Card.CheckItem> m_checkitems = _trelloutils.GetCheckListItems(m_checklist);

                        foreach (Card.CheckItem c in m_checkitems)
                        {
                            CheckBox m_check = new CheckBox();
                            m_check.Content = c.Name;
                            m_check.IsChecked = c.Checked;
                            m_check.Tag = c;
                            ListBox_CheckList.Items.Add(m_check);
                        }
                    }
                    CardChanged(new EventArgs());
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message + ": " + ex.Source); }
        }

        /// <summary>
        /// Checklist ComboBox Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_Checklists_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // populate checklist items
                if (ComboBox_Checklists.SelectedIndex > -1)
                {
                    ListBox_CheckList.Items.Clear();

                    ComboBoxItem m_cbxchecks = (ComboBoxItem)ComboBox_Checklists.SelectedItem;
                    Card.Checklist m_checklist = (Card.Checklist)m_cbxchecks.Tag;
                    List<Card.CheckItem> m_checkitems = _trelloutils.GetCheckListItems(m_checklist);

                    _selectedchecklist = m_checklist;

                    foreach (Card.CheckItem c in m_checkitems)
                    {
                        CheckBox m_check = new CheckBox();
                        m_check.Content = c.Name;
                        m_check.IsChecked = c.Checked;
                        m_check.Tag = c;
                        ListBox_CheckList.Items.Add(m_check);
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// New Check Items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_CheckNewActions_Click(object sender, RoutedEventArgs e)
        {
            RefreshUpdates();
        }

        /// <summary>
        /// Change card list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_ChangeCardList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBoxItem cbx = (ComboBoxItem)ComboBox_ChangeCardList.SelectedItem;
                TrelloNet.List newList = (TrelloNet.List)cbx.Tag;
                _changelist = newList;
            }
            catch { }

        }
        #endregion

        #region Private Members - ComboBox Selection Helpers

        /// <summary>
        /// Refresh Cards
        /// </summary>
        private void RefreshCards()
        {
            try
            {
                if (ComboBox_Lists.SelectedIndex > -1)
                {
                    ComboBox_Cards.IsEnabled = true;
                    Button_CreateCard.IsEnabled = true;
                    ComboBox_Cards.Items.Clear();

                    ComboBoxItem m_cbxitem = (ComboBoxItem)ComboBox_Lists.SelectedItem;
                    TrelloNet.List m_list = (TrelloNet.List)m_cbxitem.Tag;
                    List<Card> m_cards = _trelloutils.GetTrelloCards(m_list);

                    foreach (Card c in m_cards)
                    {
                        ComboBoxItem m_cbx = new ComboBoxItem();
                        m_cbx.Content = c.Name;
                        m_cbx.Tag = c;
                        ComboBox_Cards.Items.Add(m_cbx);
                    }
                }

                ClearCardInfo();
                EnableCardUI(false);

                Label_CardName.Text = "";
            }
            catch { }
        }

        /// <summary>
        /// Clear card contents
        /// </summary>
        private void ClearCardInfo()
        {
            try
            {
                Label_CardName.Text = "";

                ComboBox_Checklists.Items.Clear();
                ComboBox_ChangeCardList.Items.Clear();
                ComboBox_Members.Items.Clear();
                ListBox_CheckList.Items.Clear();
                WrapPanel_Labels.Children.Clear();
                StackPanel_Comments.Children.Clear();
                StackPanel_Attachments.Children.Clear();

                DatePicker_Due.SelectedDate = null;
            }
            catch { }


        }

        /// <summary>
        /// Get Change List
        /// </summary>
        private void GetChangeLists()
        {
            try
            {
                foreach (ComboBoxItem i in ComboBox_Lists.Items)
                {
                    ComboBoxItem m_cbxNew = new ComboBoxItem();
                    m_cbxNew.Content = i.Content;
                    m_cbxNew.Tag = i.Tag;
                    ComboBox_ChangeCardList.Items.Add(m_cbxNew);
                }

            }
            catch { }

        }

        /// <summary>
        /// Enable (or disable) main card UI elements
        /// </summary>
        /// <param name="Enable"></param>
        private void EnableCardUI(bool Enable)
        {
            try
            {
                ComboBox_Checklists.IsEnabled = Enable;
                ComboBox_Members.IsEnabled = Enable;
                Button_UpdateCard.IsEnabled = Enable;
                Button_AddComment.IsEnabled = Enable;
                Button_AddAttachment.IsEnabled = Enable;
                Button_AddChecklist.IsEnabled = Enable;
                ComboBox_ChangeCardList.IsEnabled = Enable;
                Button_ScreenShot.IsEnabled = Enable;
                Button_AddMembers.IsEnabled = Enable;
                Button_AddLabels.IsEnabled = Enable;
                //Button_MoveCard.IsEnabled = Enable;
            }
            catch { }

        }

        /// <summary>
        /// Select the newly created card
        /// </summary>
        private void SelectNewCard()
        {
            try
            {
                foreach (ComboBoxItem i in ComboBox_Cards.Items)
                {
                    if (i.Content.ToString() == _selectedcard.Name)
                    {
                        ComboBox_Cards.SelectedItem = i;
                        break;
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Refresh Board Updates
        /// </summary>
        private void RefreshUpdates()
        {
            try
            {
                if (ComboBox_Boards.SelectedIndex > -1)
                {
                    List<TrelloNet.Action> m_newActions = _trelloutils.GetNewActions(ref _selectedboard);

                    if (m_newActions.Count > 0)
                    {

                        MessageBox.Show((m_newActions.Count == 1 ? "There has been one change" : "There have been " + m_newActions.Count.ToString() + " changes") + " to this board since last refreshed. Board is being refreshed.", "Refresh Board");

                        string m_currentCardId = "none";
                        string m_currentListId = "none";

                        if (ComboBox_Cards.SelectedIndex > -1)
                        {
                            _selectedcard = _trelloutils.GetCardById(_selectedcard.Id);
                            _selectedlist = _trelloutils.GetListForCard(_selectedcard);
                            m_currentCardId = _selectedcard.Id;
                            m_currentListId = _selectedlist.Id;
                        }
                        else if (ComboBox_Lists.SelectedIndex > -1)
                        {
                            _selectedlist = _trelloutils.GetListById(_selectedlist.Id);
                            m_currentListId = _selectedlist.Id;
                        }

                        ComboBox_Lists.Items.Clear();
                        ComboBox_Cards.Items.Clear();

                        List<TrelloNet.List> m_lists = _trelloutils.GetTrelloLists(_selectedboard);
                        int m_selectedListIndex = -1;

                        int listIdx = 0;
                        foreach (TrelloNet.List l in m_lists)
                        {
                            ComboBoxItem m_cbx = new ComboBoxItem();
                            m_cbx.Content = l.Name;
                            m_cbx.Tag = l;
                            ComboBox_Lists.Items.Add(m_cbx);
                            if (l.Id == m_currentListId)
                            {
                                m_selectedListIndex = listIdx;
                            }
                            listIdx += 1;
                        }

                        if (m_selectedListIndex > -1)
                        {
                            ComboBox_Lists.SelectedIndex = m_selectedListIndex;
                        }

                        ComboBox_Cards.Items.Clear();
                        ClearCardInfo();
                        EnableCardUI(false);

                        List<Card> m_cards = _trelloutils.GetTrelloCards(_selectedlist);
                        int m_selectedCardIndex = -1;

                        int cardIdx = 0;
                        foreach (Card c in m_cards)
                        {
                            ComboBoxItem m_cbx = new ComboBoxItem();
                            m_cbx.Content = c.Name;
                            m_cbx.Tag = c;
                            ComboBox_Cards.Items.Add(m_cbx);
                            if (c.Id == m_currentCardId)
                            {
                                m_selectedCardIndex = cardIdx;
                            }
                            cardIdx += 1;
                        }

                        if (m_selectedCardIndex > -1)
                        {
                            EnableCardUI(true);
                            ComboBox_Cards.SelectedIndex = m_selectedCardIndex;
                        }

                    }
                    else
                    {
                        MessageBox.Show("No new changes to this board.");
                    }
                }
            }
            catch { }

        }

        #endregion

        #region AEC App Events
        public event EventHandler CheckForCardObjects;

        protected virtual void CardChanged(EventArgs e)
        {
            EventHandler handler = CheckForCardObjects;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion
    }
}
