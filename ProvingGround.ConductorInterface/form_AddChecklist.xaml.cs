﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using TrelloNet;
using ProvingGround.ConductorInterface.Classes;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for form_AddChecklist.xaml
    /// </summary>
    public partial class form_AddChecklist : Window
    {
        form_TrelloDock _parent;

        private Card _selectedCard;
        private clsTrelloUtils _trelloutils;

        public form_AddChecklist(clsTrelloUtils trelloutils, Card selectedCard, form_TrelloDock parent)
        {
            InitializeComponent();

            _parent = parent;
            _selectedCard = selectedCard;
            _trelloutils = trelloutils;
        }

        /// <summary>
        /// Add Checklist Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddChecklist_Click(object sender, RoutedEventArgs e)
        {

            List<CheckBox> m_checklistitems = new List<CheckBox>();
            foreach (CheckBox cb in ListBox_CheckItem.Items)
            {
                m_checklistitems.Add(cb);
            }

            _trelloutils.AddChecklist(_selectedCard, TextBox_NewChecklist.Text, m_checklistitems);
          
            this.Close();
        }

        /// <summary>
        /// Add a Checklist item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox_Checkitem.Text != "")
            {
                CheckBox m_lbitem = new CheckBox();
                m_lbitem.Content = TextBox_Checkitem.Text;
                TextBox_Checkitem.Text = "";
                ListBox_CheckItem.Items.Add(m_lbitem);
            }
        }

        /// <summary>
        /// Remove a Checklist item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Remove_Click(object sender, RoutedEventArgs e)
        {
            if (ListBox_CheckItem.SelectedItem != null)
            {
                ListBox_CheckItem.Items.Remove(ListBox_CheckItem.SelectedItem);
            }
        }

        /// <summary>
        /// Close the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void TextBox_Checkitem_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Checkitem.Text == "Item description...")
            {
                TextBox_Checkitem.Text = "";
            }
        }

        private void TextBox_Checkitem_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Checkitem.Text == "")
            {
                TextBox_Checkitem.Text = "Item description...";
            }
        }

        private void TextBox_NewChecklist_GotFocus(object sender, RoutedEventArgs e)
        {
            if(TextBox_NewChecklist.Text == "Name of the checklist...")
            {
                TextBox_NewChecklist.Text = "";
            }
        }

        private void TextBox_NewChecklist_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_NewChecklist.Text == "")
            {
                TextBox_NewChecklist.Text = "Name of the checklist...";
            }
        }
    }
}
