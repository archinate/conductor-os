﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using TrelloNet;
using ProvingGround.ConductorInterface.Classes;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for form_AddLabel.xaml
    /// </summary>
    public partial class form_AddLabel : Window
    {
        private clsTrelloUtils _trelloUtils;
        private Board _selectedBoard;
        private Card _selectedCard;
        private form_TrelloDock _parent;

        public form_AddLabel(clsTrelloUtils trelloutils, Board selectedBoard, Card selectedCard, form_TrelloDock parent)
        {
            InitializeComponent();

            _trelloUtils = trelloutils;
            _selectedBoard = selectedBoard;
            _selectedCard = selectedCard;
            _parent = parent;
            DoSetup();
        }

        /// <summary>
        /// Setup the form
        /// </summary>
        private void DoSetup()
        {
            try
            {
                List<clsLabel> m_boardLabels = _trelloUtils.GetLabelsForBoard(_selectedBoard);

                foreach (clsLabel thisLabel in m_boardLabels)
                {
                    CheckBox addLabel = new CheckBox() { Content = new uc_Label(thisLabel.Name, thisLabel.Color) };
                    addLabel.Tag = thisLabel;
                    ListBox_Labels.Items.Add(addLabel);
                    foreach (Card.Label cl in _selectedCard.Labels)
                    {
                        
                        if (cl.Name == thisLabel.Name && cl.Color == thisLabel.Color)
                        {
                            addLabel.IsChecked = true;
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Close the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Add Label
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddLabels_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (CheckBox c in ListBox_Labels.Items)
                {
                    clsLabel lab = (clsLabel)c.Tag;
                    string l = lab.Id;
                    if (c.IsChecked == true)
                    {
                        _trelloUtils.AddLabel(_selectedCard, l);
                    }
                    else if (c.IsChecked == false)
                    {
                        _trelloUtils.RemoveLabel(_selectedCard, l);
                    }
                }

                this.Close();
            }
            catch { this.Close(); }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
