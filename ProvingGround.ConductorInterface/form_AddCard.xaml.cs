﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using System.Data;
using System.IO;
using System.Xml;

using ProvingGround.ConductorInterface.Classes;

using TrelloNet;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for form_Main.xaml
    /// </summary>
    public partial class form_AddCard : Window
    {
        private clsTrelloUtils _trelloutils;
        private TrelloNet.List _trellolist;
        private TrelloNet.Board _trelloboard;

        private TrelloNet.Card _newcard;

        #region Public Properties
        /// <summary>
        /// Newly Created Card
        /// </summary>
        public TrelloNet.Card NewCard
        {
            get
            {
                return _newcard;
            }
        }
        #endregion

        public form_AddCard(clsTrelloUtils trelloutils, Board trelloboard, TrelloNet.List trellolist)
        {
            InitializeComponent();

            //widen scope
            _trelloutils = trelloutils;
            _trellolist = trellolist;
            _trelloboard = trelloboard;

            //setup card
            DoSetup();

        }

        /// <summary>
        /// Create a new Trello Card
        /// </summary>
        private void CreateCard()
        {
            //card creation info
            TrelloNet.List m_trellolist = _trellolist;

            DateTime? m_duedate = null;
            try
            {
                m_duedate = DatePicker_Due.SelectedDate.Value;
            }
            catch { }

            string m_name = TextBox_Title.Text;
            string m_desc = TextBox_Description.Text;

            List<Member> m_members = new List<Member>();
            foreach (CheckBox c in ListBox_Member.Items)
            {
                if (c.IsChecked == true)
                {
                    m_members.Add((Member)c.Tag);
                }
            }

            List<string> m_labels = new List<string>();
            foreach (CheckBox c in ListBox_Labels.Items)
            {
                if (c.IsChecked == true)
                {
                    clsLabel lab = (clsLabel)c.Tag;
                    m_labels.Add(lab.Id);
                }
            }

            List<CheckBox> m_checklistitems = new List<CheckBox>();
            foreach (CheckBox cb in ListBox_Checkitem.Items)
            {
                m_checklistitems.Add(cb);
            }

            //Create the card
            _newcard = _trelloutils.CreateCard(m_trellolist, m_name, m_desc, m_members, m_duedate, m_checklistitems, m_labels);

            if (CheckBox_Screenshot.IsChecked == true)
            {
                TakeScreenshot(_newcard);
            }
        }

        /// <summary>
        /// Take a screenshot
        /// </summary>
        private void TakeScreenshot(Card card)
        {
            DateTime m_timeStamp = DateTime.Now;
            string fileName = m_timeStamp.ToString().Replace("/","-").Replace(":","-") + ".jpeg";
            string tempDir = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Proving Ground", "Conductor", "temp");
            if (Directory.Exists(tempDir) == false)
            {
                Directory.CreateDirectory(tempDir);
            }

            string path = System.IO.Path.Combine(tempDir, fileName);
            form_Screen m_screen = new form_Screen(path, _trelloutils, card);
            m_screen.Show();
        }

        /// <summary>
        /// Set up the form
        /// </summary>
        private void DoSetup()
        {
            ListBox_Member.Items.Clear();
            List<Member> m_members = _trelloutils.GetBoardMembers(_trelloboard);
            
            foreach (Member m in m_members)
            {
                CheckBox m_cbx = new CheckBox();
                m_cbx.Content = m.FullName;
                m_cbx.Tag = m;
                ListBox_Member.Items.Add(m_cbx);
            }

            DatePicker_Due.SelectedDate = DateTime.Today;
            DatePicker_Due.DisplayDate = DateTime.Today;

            List<clsLabel> m_boardLabels = _trelloutils.GetLabelsForBoard(_trelloboard);

            foreach (clsLabel thisLabel in m_boardLabels)
            {
                CheckBox addLabel = new CheckBox() { Content = new uc_Label(thisLabel.Name, thisLabel.Color) };
                addLabel.Tag = thisLabel;
                ListBox_Labels.Items.Add(addLabel);
            }

        }

        #region Form Events
        /// <summary>
        /// Close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Run code here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Run_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            CreateCard();
            
        }

        /// <summary>
        /// Hyperlink
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        /// <summary>
        /// Drag Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// Add a Checklist item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox_Checkitem.Text != "")
            {
                CheckBox m_lbitem = new CheckBox();
                m_lbitem.Content = TextBox_Checkitem.Text;
                TextBox_Checkitem.Text = "";
                ListBox_Checkitem.Items.Add(m_lbitem);
            }
        }

        /// <summary>
        /// Remove a Checklist item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Remove_Click(object sender, RoutedEventArgs e)
        {
            if (ListBox_Checkitem.SelectedItem != null)
            {
                ListBox_Checkitem.Items.Remove(ListBox_Checkitem.SelectedItem);
            }
        }


        private void TextBox_Title_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Title.Text == "")
            {
                TextBox_Title.Text = "Card title...";
            }
        }

        private void TextBox_Title_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Title.Text == "Card title...")
            {
                TextBox_Title.Text = "";
            }
        }

        private void TextBox_Description_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Description.Text == "")
            {
                TextBox_Description.Text = "Add a description...";
            }
        }

        private void TextBox_Description_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Description.Text == "Add a description...")
            {
                TextBox_Description.Text = "";
            }
        }

        private void TextBox_Checkitem_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Checkitem.Text == "")
            {
                TextBox_Checkitem.Text = "Item description...";
            }
        }

        private void TextBox_Checkitm_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TextBox_Checkitem.Text == "Item description...")
            {
                TextBox_Checkitem.Text = "";
            }
        }
        #endregion

    }
}
