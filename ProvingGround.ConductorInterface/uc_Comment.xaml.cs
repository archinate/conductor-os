﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for uc_Comment.xaml
    /// </summary>
    public partial class uc_Comment : UserControl
    {
        public uc_Comment(string MemberName, string CommentTime, string CommentValue)
        {
            InitializeComponent();

            Label_Member.Content = MemberName;
            Label_Time.Content = CommentTime;
            TextBlock_Comment.Text = CommentValue;
        }
    }
}
