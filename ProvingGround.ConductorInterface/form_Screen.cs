﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;

using ProvingGround.ConductorInterface.Classes;
using TrelloNet;

namespace ProvingGround.ConductorInterface
{
    public partial class form_Screen : Form
    {
        //These variables control the mouse position
        int selectX;
        int selectY;
        int selectWidth;
        int selectHeight;
        public Pen selectPen;

        //This variable control when you start the right click
        bool start = false;

        private string _path;

        private clsTrelloUtils _trelloUtils;
        private Card _card;

        public form_Screen(string path, clsTrelloUtils trelloUtils, Card card)
        {
            InitializeComponent();
            _path = path;

            _trelloUtils = trelloUtils;
            _card = card;
        }

        /// <summary>
        /// On Form Activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnActivated(object sender, EventArgs e)
        {

            this.Top = 0;
            this.Left = 0;

            //Hide the Form
            this.Hide();
            //Create the Bitmap
            Bitmap printscreen = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                                     Screen.PrimaryScreen.Bounds.Height);
            //Create the Graphic Variable with screen Dimensions
            Graphics graphics = Graphics.FromImage(printscreen as Image);
            //Copy Image from the screen
            graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);
            //Create a temporal memory stream for the image
            using (MemoryStream s = new MemoryStream())
            {
                //save graphic variable into memory
                printscreen.Save(s, ImageFormat.Bmp);
                pictureBox_Capture.Size = new System.Drawing.Size(this.Width, this.Height);
                //set the picture box with temporary stream
                pictureBox_Capture.Image = Image.FromStream(s);
            }

            //Cross Cursor
            Cursor = Cursors.Cross;
            //Show Form
            this.Show();
        }

        /// <summary>
        /// Mouse drag event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_Capture_MouseMove(object sender, MouseEventArgs e)
        {
            //validate if there is an image
            if (pictureBox_Capture.Image == null)
                return;
            //validate if right-click was trigger
            if (start)
            {
                //refresh picture box
                pictureBox_Capture.Refresh();
                //set corner square to mouse coordinates
                selectWidth = e.X - selectX;
                selectHeight = e.Y - selectY;
                //draw dotted rectangle
                pictureBox_Capture.CreateGraphics().DrawRectangle(selectPen,
                          selectX, selectY, selectWidth, selectHeight);
            }
        }

        /// <summary>
        /// Mouse down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_Capture_MouseDown(object sender, MouseEventArgs e)
        {
            //validate when user right-click
            if (!start)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    //starts coordinates for rectangle
                    selectX = e.X;
                    selectY = e.Y;
                    selectPen = new Pen(System.Drawing.Color.Red, 3);
                    selectPen.DashStyle = DashStyle.Dash;
                }
                //refresh picture box
                pictureBox_Capture.Refresh();
                //start control variable for draw rectangle
                start = true;
            }
            else
            {
                //validate if there is image
                if (pictureBox_Capture.Image == null)
                    return;
                //same functionality when mouse is over
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    pictureBox_Capture.Refresh();
                    selectWidth = e.X - selectX;
                    selectHeight = e.Y - selectY;
                    pictureBox_Capture.CreateGraphics().DrawRectangle(selectPen, selectX,
                             selectY, selectWidth, selectHeight);

                }
                start = false;
                //function save image to clipboard
                SaveImageFile();
            }
        }

        /// <summary>
        /// Save the Image File and Upload It
        /// </summary>
        private void SaveImageFile()
        {
            try
            {
                //validate if something selected
                if (selectWidth > 0)
                {

                    Rectangle rect = new Rectangle(selectX, selectY, selectWidth, selectHeight);
                    //create bitmap with original dimensions
                    Bitmap OriginalImage = new Bitmap(pictureBox_Capture.Image, pictureBox_Capture.Width, pictureBox_Capture.Height);
                    //create bitmap with selected dimensions
                    Bitmap _img = new Bitmap(selectWidth, selectHeight);
                    //create graphic variable
                    Graphics g = Graphics.FromImage(_img);
                    //set graphic attributes
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.DrawImage(OriginalImage, 0, 0, rect, GraphicsUnit.Pixel);
                    //g.Dispose();
                    //insert image stream into clipboard
                    _img.Save(_path, ImageFormat.Jpeg);

                    //upload to trello
                    _trelloUtils.AddAttachment(_card, _path);
                    File.Delete(_path);

                    
                }
                //End application
                this.Close();
                MessageBox.Show("The image was added to the Trello card.", "Image created.", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString(), "Error creating image.", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}
