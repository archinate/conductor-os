﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ProvingGround.TrelloInterface.Classes;

namespace ProvingGround.TrelloInterface
{
    /// <summary>
    /// Interaction logic for form_EditLabels.xaml
    /// </summary>
    public partial class form_EditLabels : Window
    {



        public form_EditLabels(List<clsLabel> labels)
        {
            InitializeComponent();

            //widen scope

            foreach (clsLabel thisLabel in labels)
            {
                WrapPanel thisWrap = new WrapPanel();
                thisWrap.Orientation = Orientation.Horizontal;
                thisWrap.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                thisWrap.Children.Add(new uc_Label(thisLabel.Name, thisLabel.Color));
                thisWrap.Children.Add(new CheckBox() { IsEnabled=true});
                StackPanel_Main.Children.Add(thisWrap);
            }

        }

        
        

    }
}
