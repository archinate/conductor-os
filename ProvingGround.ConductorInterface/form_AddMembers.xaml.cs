﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using TrelloNet;
using ProvingGround.ConductorInterface.Classes;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for form_AddMembers.xaml
    /// </summary>
    public partial class form_AddMembers : Window
    {
        private clsTrelloUtils _trelloUtils;
        private Board _selectedBoard;
        private Card _selectedCard;
        private form_TrelloDock _parent;

        public form_AddMembers(clsTrelloUtils trelloutils, Board selectedBoard, Card selectedCard, form_TrelloDock parent)
        {
            InitializeComponent();
            _trelloUtils = trelloutils;
            _selectedBoard = selectedBoard;
            _selectedCard = selectedCard;
            _parent = parent;
            DoSetup();
        }

        /// <summary>
        /// Setup
        /// </summary>
        private void DoSetup()
        {
            try
            {
                ListBox_Members.Items.Clear();
                List<Member> m_members = _trelloUtils.GetBoardMembers(_selectedBoard);

                foreach (Member m in m_members)
                {
                    CheckBox m_cbx = new CheckBox();
                    m_cbx.Content = m.FullName;
                    m_cbx.Tag = m;
                    foreach (String cm in _selectedCard.IdMembers)
                    {
                        if (cm == m.Id)
                        {
                            m_cbx.IsChecked = true;
                        }
                    }
                    ListBox_Members.Items.Add(m_cbx);
                }
            }
            catch { }

        }

        /// <summary>
        /// Add Members to Card
        /// </summary>
        private void AddMembersToCard()
        {
            foreach (CheckBox c in ListBox_Members.Items)
            {
                if (c.IsChecked == true)
                {
                    _trelloUtils.AddMember(_selectedCard, (Member)c.Tag);
                }
                else if (c.IsChecked == false)
                {
                    _trelloUtils.RemoveMember(_selectedCard, (Member)c.Tag);
                }
            }
        }

        /// <summary>
        /// Add members to boards
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddMembers_Click(object sender, RoutedEventArgs e)
        {
            AddMembersToCard();
            this.Close();
        }

        /// <summary>
        /// Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

    }
}
