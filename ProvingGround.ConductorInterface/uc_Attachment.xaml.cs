﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for uc_Attachment.xaml
    /// </summary>
    public partial class uc_Attachment : UserControl
    {
        private string _url;
        private string _fileName;

        public uc_Attachment(string FileName, string URL, bool downloadable)
        {
            InitializeComponent();

            _fileName = FileName;
            _url = URL;

            Label_FileName.Content = FileName;
            Button_Download.IsEnabled = downloadable;
            if (!downloadable)
            {
                Button_Download.Content = "Newly Added";
            }
        }

        private void Button_Download_Click(object sender, RoutedEventArgs e)
        {
            System.Net.WebClient m_downloader = new System.Net.WebClient();

            System.Windows.Forms.SaveFileDialog m_saveDialog = new System.Windows.Forms.SaveFileDialog();
            m_saveDialog.FileName = _fileName;

            if (m_saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    m_downloader.DownloadFile(_url, m_saveDialog.FileName);
                    System.Windows.Forms.MessageBox.Show("Attachment saved to " + m_saveDialog.FileName);
                }
                catch (Exception)
                {
                    System.Windows.Forms.MessageBox.Show("Unable to download file");
                }
            }
            
        }
    }
}
