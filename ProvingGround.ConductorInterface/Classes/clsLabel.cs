﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.ConductorInterface.Classes
{
    /// <summary>
    /// Simple class for deserializing json data from a get call to the trello service
    /// </summary>
    public class clsLabel
    {
        public string Id { get; set; }
        public string IdBoard { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int uses { get; set; }

        public clsLabel() { }
    }
}
