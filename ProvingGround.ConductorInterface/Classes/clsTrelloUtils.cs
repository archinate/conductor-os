﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Threading;
using System.Net;

using TrelloNet;
using TrelloNet.Extensions;

using Newtonsoft.Json;

namespace ProvingGround.ConductorInterface.Classes
{
    /// <summary>
    /// Helper Class for Trello
    /// </summary>
    public class clsTrelloUtils
    {
        private ITrello _trello;
        private string _tk = "eacbb7917db670e7f9dc1ab01d0d37ef";
        private DateTime _lastTime;


        #region Public Properties
        /// <summary>
        /// My Membership
        /// </summary>
        public Member MyMembership
        {
            get
            {
                return _trello.Members.Me();
            }
        }

        /// <summary>
        /// Authenticated Member Name
        /// </summary>
        public string AuthMemberName
        {
            get
            {
                try
                {
                    Member m_me = _trello.Members.Me();
                    string m_name = m_me.FullName;
                    return m_name;
                }
                catch (Exception ex){ return ""; }           
            }
        }

        /// <summary>
        /// List of all my Organizations
        /// </summary>
        public List<Organization> AllOrganizations
        {
            get
            {
                IEnumerable<Organization> m_orgs = _trello.Organizations.ForMe(OrganizationFilter.All);
                return m_orgs.ToList();
            }
        }

        /// <summary>
        /// List of all of my Boards
        /// </summary>
        public List<Board> OpenBoards
        {
            get
            {
                IEnumerable<Board> m_boards = _trello.Boards.ForMe(BoardFilter.Open);
                return m_boards.ToList();
            }
        }

        /// <summary>
        /// Time of the last check for updates
        /// </summary>
        public DateTime LastUpdate
        {
            get
            {
                return _lastTime;
            }
            set
            {
                _lastTime = value;
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public clsTrelloUtils()
        {
            //widen scope
            SetupTrello();
        }

        #region Public Members - Access
        /// <summary>
        /// Get Members on a Board
        /// </summary>
        /// <param name="board">Trello Board</param>
        /// <returns>List of Members</returns>
        public List<Member> GetBoardMembers(Board board)
        {
            try
            {
                IEnumerable<Member> m_members = _trello.Members.ForBoard(board, MemberFilter.All);
                return m_members.ToList();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a list of Trello boards by Organization
        /// </summary>
        /// <param name="org">A Trello Organization</param>
        /// <returns></returns>
        public List<Board> GetTrelloBoardsbyOrganization(Organization org)
        {
            try
            {
                IEnumerable<Board> m_boards = _trello.Boards.ForOrganization(org, BoardFilter.Open);
                return m_boards.ToList();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a list of Trello Lists on a Board
        /// </summary>
        /// <param name="trelloboard">Trello Board</param>
        /// <returns>List of Lists</returns>
        public List<List> GetTrelloLists(Board trelloboard)
        {
            try
            {
                IEnumerable<List> m_lists = _trello.Lists.ForBoard(trelloboard, ListFilter.Open);
                return m_lists.ToList();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a list of Trello Cards from a List
        /// </summary>
        /// <param name="trellolist">Trello List</param>
        /// <returns>List of Cards</returns>
        public List<Card> GetTrelloCards(List trellolist)
        {
            try
            {
                IEnumerable<Card> m_cards = _trello.Cards.ForList(trellolist, CardFilter.Open);
                return m_cards.ToList();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get Trello checklists
        /// </summary>
        /// <param name="card">A Trello card</param>
        /// <returns>A list of Checklists</returns>
        public List<Card.Checklist> GetTrelloCheckLists(Card card)
        {
            try
            {
                return card.Checklists;
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a list of members on a card
        /// </summary>
        /// <param name="card">Trello Card</param>
        /// <returns>List of Members</returns>
        public List<string> GetTrelloMembersOnCard(Card card)
        {
            try
            {
                List<string> m_ids = card.IdMembers;
                List<string> m_names = new List<string>();
                foreach (string m in m_ids)
                {
                    Member mem = _trello.Members.WithId(m);
                    m_names.Add(mem.FullName);
                }

                return m_names;
            }
            catch { return null; }
        }

        /// <summary>
        /// Get a card from its CardId
        /// </summary>
        /// <param name="CardId"></param>
        /// <returns></returns>
        public Card GetCardById(string CardId)
        {
            try
            {
                return _trello.Cards.WithId(CardId);
            }
            catch { return null; }

        }

        /// <summary>
        /// Get a list from its ListId
        /// </summary>
        /// <param name="ListId"></param>
        /// <returns></returns>
        public TrelloNet.List GetListById(string ListId)
        {
            try
            {
                return _trello.Lists.WithId(ListId);
            }
            catch { return null; }

        }

        /// <summary>
        /// Get Trello Checklist items
        /// </summary>
        /// <param name="checklist"></param>
        /// <returns>A list of Checklist items</returns>
        public List<Card.CheckItem> GetCheckListItems(Card.Checklist checklist)
        {
            try
            {
                List<Card.CheckItem> m_checkitems = checklist.CheckItems;
                return m_checkitems;
            }
            catch { return null; }
        }



        /// <summary>
        /// Direct query to the database
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public List<clsLabel> GetLabelsForBoard(Board board)
        {
            try
            {
                // https://developers.trello.com/advanced-reference/
                dynamic getLabels = _trello.Advanced.Get("/boards/" + board.Id + "/labels", new { labels = "all" });

                // clsLabel defined to match structure acquired from the get call
                var theseLabels = JsonConvert.DeserializeObject<List<clsLabel>>(getLabels.ToString());

                List<clsLabel> returnLabels = new List<clsLabel>();

                foreach (clsLabel thisLabel in theseLabels)
                {
                    returnLabels.Add(thisLabel);
                }

                returnLabels = returnLabels.OrderBy(c => c.Color).ThenBy(n => n.Name).ToList();

                return returnLabels;
            }
            catch { return null; }

        }

        /// <summary>
        /// Gets the date and time a card was created
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public DateTime GetCardCreatedDate(Card card)
        {
            DateTime dt = DateTime.Now;
            try
            {
                foreach (TrelloNet.Action cardCreated in _trello.Actions.ForCard((ICardId)card, new List<ActionType>() { ActionType.CreateCard }))
                {
                    return cardCreated.Date.ToLocalTime();
                }
                return DateTime.Now;
            }
            catch { return dt; }

        }

        /// <summary>
        /// Get Trello CommentCardAction items
        /// </summary>
        /// <param name="card"></param>
        /// <returns>A list of CommentCardAction items</returns>
        public List<CommentCardAction> GetCardComments(Card card)
        {
            try
            {
                List<CommentCardAction> comments =
    _trello.Actions.ForCard(card, new[] { ActionType.CommentCard }).Cast<CommentCardAction>().ToList();
                return comments;
            }
            catch { return null; }

        }



        #endregion

        #region Public Members - Authorization
        /// <summary>
        /// Authorize the Application
        /// </summary>
        public Uri AuthorizeURL()
        {
            try
            {
                Uri m_url = _trello.GetAuthorizationUrl("Conductor - Open Source", Scope.ReadWrite);
                return m_url;
            }
            catch { return null; }
        }

        /// <summary>
        /// Authorize the App
        /// </summary>
        /// <param name="token">The token from the authorization website</param>
        /// <returns>True if successful</returns>
        public bool Authorize(string token)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                _trello.Authorize(token);
                return true;
            }
            catch { return false; }
        }
        #endregion

        #region Public Members - Create or Update
        /// <summary>
        /// Create a new Trello Card on a List
        /// </summary>
        /// <param name="trellolist">Trello List</param>
        /// <param name="name">Name of the card</param>
        /// <param name="description">Card Description</param>
        /// <param name="member">Member assigned to the card.</param>
        /// <param name="due">Due date for the card.</param>
        public Card CreateCard(List trellolist, string name, string description, List<Member> members, DateTime? due, List<CheckBox> checklistitems, List<string> labels)
        {
            try
            {
                // Create card with data
                Board m_board = _trello.Boards.WithId(trellolist.IdBoard);
                Card m_newcard = _trello.Cards.Add(new NewCard(name, trellolist));
                foreach (Member member in members)
                {
                    _trello.Cards.AddMember(m_newcard, member);
                }

                foreach (string l in labels)
                {
                    _trello.Cards.AddLabel(m_newcard, l);
                }


                if (due != null)
                {
                    _trello.Cards.ChangeDueDate(m_newcard, due);
                }

                if (description != "")
                {
                    _trello.Cards.ChangeDescription(m_newcard, description);
                }

                // Add Checklist
                if (checklistitems.Count > 0)
                {
                    Checklist m_checklist = _trello.Checklists.Add("My Checklist", m_newcard);
                    foreach (CheckBox cb in checklistitems)
                    {
                        _trello.Checklists.AddCheckItem(m_checklist, cb.Content.ToString());
                    }
                    _trello.Cards.AddChecklist(m_newcard, m_checklist);
                }

                return m_newcard;
            }
            catch { return null; }
        }

        /// <summary>
        /// Add Checklist
        /// </summary>
        /// <param name="card"></param>
        /// <param name="name"></param>
        /// <param name="checklistitems"></param>
        public void AddChecklist(Card card, string name, List<CheckBox> checklistitems)
        {
            if (checklistitems.Count > 0)
            {
                Checklist m_checklist = _trello.Checklists.Add(name, card);
                foreach (CheckBox cb in checklistitems)
                {
                    _trello.Checklists.AddCheckItem(m_checklist, cb.Content.ToString());
                }
                _trello.Cards.AddChecklist(card, m_checklist);
            }
        }

        /// <summary>
        /// Move card to a specified list
        /// </summary>
        /// <param name="moveTo"></param>
        /// <param name="moveCard"></param>
        public void Movecard(List moveTo, Card moveCard)
        {
            try
            {
                _trello.Async.Cards.Move((ICardId)moveCard, (IListId)moveTo);
            }
            catch { }
        }

        /// <summary>
        /// Update a Card with new info
        /// </summary>
        /// <param name="checlistitems">List of Checklist items</param>
        /// <param name="due">Due date</param>
        /// <returns>Updated card</returns>
        public Card UpdateCard(Card updatedcard, Card.Checklist checklist, List<CheckBox> checklistitems, DateTime? due)
        {
            try
            {
                _trello.Cards.ChangeDueDate(updatedcard, due);

                foreach (CheckBox cb in checklistitems)
                {
                    Card.CheckItem m_ci = (Card.CheckItem)cb.Tag;
                    ICardId m_cardid = updatedcard;
                    IChecklistId m_checklistid = checklist;
                    ICheckItemId m_checkitemid = m_ci;

                    _trello.Async.Cards.ChangeCheckItemState(m_cardid, m_checklistid, m_checkitemid, cb.IsChecked.Value);
                }
                return updatedcard;
            }
            catch { return null; }
        }

        /// <summary>
        /// Update a Card with new info
        /// </summary>
        /// <param name="checlistitems">List of Checklist items</param>
        /// <param name="due">Due date</param>
        /// <returns>Updated card</returns>
        public Card UpdateCard(Card updatedcard, Card.Checklist checklist, List<CheckBox> checklistitems)
        {
            try
            {

                foreach (CheckBox cb in checklistitems)
                {
                    Card.CheckItem m_ci = (Card.CheckItem)cb.Tag;
                    ICardId m_cardid = updatedcard;
                    IChecklistId m_checklistid = checklist;
                    ICheckItemId m_checkitemid = m_ci;

                    _trello.Async.Cards.ChangeCheckItemState(m_cardid, m_checklistid, m_checkitemid, cb.IsChecked.Value);
                }
                return updatedcard;
            }
            catch { return null; }
        }

        /// <summary>
        /// Add Attachment
        /// </summary>
        /// <param name="card"></param>
        /// <param name="source"></param>
        public void AddAttachment(Card card, string source)
        {
            try
            {
                FileAttachment m_attachment = new FileAttachment(source);

                if (System.IO.File.Exists(source))
                {
                    try
                    {
                        _trello.Cards.AddAttachment((ICardId)card, m_attachment);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show("Unable to upload " + source + ": " + ex.Message);
                    }
                }
            }
            catch { }

        }

        /// <summary>
        /// Add Comment
        /// </summary>
        /// <param name="card"></param>
        /// <param name="comment"></param>
        public void AddComment(Card card, string comment)
        {
            try
            {
                _trello.Cards.AddComment((ICardId)card, comment);
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Add label to card
        /// </summary>
        /// <param name="card"></param>
        /// <param name="label"></param>
        public void AddLabel(Card card, string label)
        {
            try
            {
                _trello.Cards.AddLabel((ICardId)card, label);
            }
            catch { }
        }

        /// <summary>
        /// Remove label from card
        /// </summary>
        /// <param name="card"></param>
        /// <param name="label"></param>
        public void RemoveLabel(Card card, string label)
        {
            try
            {
                _trello.Cards.RemoveLabel((ICardId)card, label);
            }
            catch {  }
        }

        /// <summary>
        /// Add member to card
        /// </summary>
        /// <param name="card"></param>
        /// <param name="member"></param>
        public void AddMember(Card card, Member member)
        {
            try
            {
                _trello.Cards.AddMember((ICardId)card, (IMemberId)member);
            }
            catch { }

        }

        /// <summary>
        /// Remove Member from card
        /// </summary>
        /// <param name="card"></param>
        /// <param name="member"></param>
        public void RemoveMember(Card card, Member member)
        {
            try
            {
                _trello.Cards.RemoveMember((ICardId)card, (IMemberId)member);
            }
            catch { }
        }
        #endregion

        #region Public Members - Refresh

        /// <summary>
        /// Check for changes
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public List<TrelloNet.Action> GetNewActions(ref Board board)
        {
            try
            {
                List<TrelloNet.Action> m_newActions = new List<TrelloNet.Action>();

                foreach (TrelloNet.Action thisAction in _trello.Actions.AutoPaged().ForBoard(board, since: Since.Date(_lastTime)))
                {
                    m_newActions.Add(thisAction);
                }

                _lastTime = DateTime.Now;

                board = _trello.Boards.WithId(board.Id);

                return m_newActions;
            }
            catch { return null; }

        }

        public TrelloNet.List GetListForCard(Card card)
        {
            try
            {
                return _trello.Lists.ForCard((ICardId)card);
            }
            catch { return null; }

        }

        #endregion

        #region Private Members - Setup
        /// <summary>
        /// Setup Trello
        /// </summary>
        private void SetupTrello()
        {
            try
            {
                _trello = new Trello(_tk);
                _lastTime = DateTime.Now;
            }
            catch { }
        }


        #endregion
    }

}
