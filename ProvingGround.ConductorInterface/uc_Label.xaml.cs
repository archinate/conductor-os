﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for uc_Label.xaml
    /// </summary>
    public partial class uc_Label : UserControl
    {
        public uc_Label(string LabelContent, string LabelColor)
        {
            InitializeComponent();
            

            SolidColorBrush _green = new SolidColorBrush(Color.FromRgb(97, 189, 79));
            SolidColorBrush _yellow = new SolidColorBrush(Color.FromRgb(242, 214, 0));
            SolidColorBrush _orange = new SolidColorBrush(Color.FromRgb(255, 171, 74));
            SolidColorBrush _red = new SolidColorBrush(Color.FromRgb(235, 90, 70));
            SolidColorBrush _purple = new SolidColorBrush(Color.FromRgb(195, 119, 224));
            SolidColorBrush _blue = new SolidColorBrush(Color.FromRgb(0, 121, 191));
            SolidColorBrush _lime = new SolidColorBrush(Color.FromRgb(81, 232, 152));
            SolidColorBrush _pink = new SolidColorBrush(Color.FromRgb(255, 128, 206));
            SolidColorBrush _charcoal = new SolidColorBrush(Color.FromRgb(77, 77, 77));

            LabelContent = LabelContent != "" ? LabelContent : "          ";
            this.Label_LabelName.Content = LabelContent;

            switch (LabelColor)
            {
                case "green":
                    this.Border_Body.Background = _green;
                    break;
                case "yellow":
                    this.Border_Body.Background = _yellow;
                    break;
                case "orange":
                    this.Border_Body.Background = _orange;
                    break;
                case "red":
                    this.Border_Body.Background = _red;
                    break;
                case "purple":
                    this.Border_Body.Background = _purple;
                    break;
                case "blue":
                    this.Border_Body.Background = _blue;
                    break;
                case "lime":
                    this.Border_Body.Background = _lime;
                    break;
                case "pink":
                    this.Border_Body.Background = _pink;
                    break;
                case "black":
                    this.Border_Body.Background = _charcoal;
                    break;
                default:
                    break;
            }
           
        }
    }
}
