﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using TrelloNet;
using ProvingGround.ConductorInterface.Classes;

namespace ProvingGround.ConductorInterface
{
    /// <summary>
    /// Interaction logic for form_AddComment.xaml
    /// </summary>
    public partial class form_AddComment : Window
    {

        private clsTrelloUtils _trelloUtils;
        private Card _selectedCard;
        private form_TrelloDock _parent;

        public form_AddComment(clsTrelloUtils trelloutils, Card selectedCard, form_TrelloDock parent)
        {
            InitializeComponent();

            _trelloUtils = trelloutils;
            _selectedCard = selectedCard;
            _parent = parent;
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Button_AddComment_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox_NewComment.Text != "")
	        {
                try
                {
                    _trelloUtils.AddComment(_selectedCard, TextBox_NewComment.Text);
                    _parent.StackPanel_Comments.Children.Add(new uc_Comment(_trelloUtils.AuthMemberName, DateTime.Now.ToLocalTime().ToString("dd-MMM h:mm tt"), TextBox_NewComment.Text));
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show("Unable to add comment: " + ex.Message);
                }
                this.Close();
	        }
            else
            {
                System.Windows.Forms.MessageBox.Show("Please add a comment...");
            }
            

            
        }
    }
}
