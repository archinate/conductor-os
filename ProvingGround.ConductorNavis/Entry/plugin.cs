﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms.Integration;
using Autodesk.Navisworks.Api.Plugins;

using ProvingGround.ConductorInterface;

namespace ProvingGround.ConductorNavis
{
    [Plugin("ProvingGround.ConductorNavis.plugin", "ProvingGround",
        DisplayName = "Conductor for Navisworks",
        ToolTip = "A dockable interface for interacting with Trello.")]
    [DockPanePlugin(150, 200, FixedSize = false)]
    class plugin : DockPanePlugin
    {
        public override System.Windows.Forms.Control CreateControlPane()
        {
            //create an ElementHost
            ElementHost eh = new ElementHost();

            //assign the control
            eh.AutoSize = true;
            eh.Child = new form_TrelloDock();

            eh.CreateControl();

            //return the ElementHost
            return eh;
        }

        public override void DestroyControlPane(System.Windows.Forms.Control pane)
        {
            pane.Dispose();
        }
    }
}
