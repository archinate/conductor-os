﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Autodesk.Navisworks.Api.Plugins;

namespace ProvingGround.ConductorNavis
{
    // plugin
    [Plugin("ProvingGround.ConductorNavis.addin", "ProvingGround", DisplayName = "Proving Ground addins")]

    //attributes
    //[Strings("CustomRibbon.name")]
    [RibbonLayout("CustomRibbon.xaml")]

    // ribbon tab
    [RibbonTab("ID_Tab_1", DisplayName = "Proving Ground")]

    // command
    [Command("ID_Tab_1_Panel_1_Button_1",
        Icon = "Template_16.png",
        LargeIcon = "Template_32.png",
        DisplayName = "Conductor",
        ToolTip = "An interface for interacting with Trello tasks.",
        ExtendedToolTip = "Conductor creates an interface for managing Trello tasks within Navisworks.")]
    public class addin : CommandHandlerPlugin
    {
        /// <summary>
        /// Execute Command
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public override int ExecuteCommand(string name, params string[] parameters)
        {
            switch (name)
            {
                case "ID_Tab_1_Panel_1_Button_1":
                    {
                        LaunchPlugin();
                        break;
                    }

                default:
                    {
                        MessageBox.Show("You have clicked on the command with ID = '" + name + "'");
                        break;
                    }
            }

            return 0;
        }

        /// <summary>
        /// Launch Main Command
        /// </summary>
        public void LaunchPlugin()
        {

            // Don't Support Automation
            if (Autodesk.Navisworks.Api.Application.IsAutomated)
            {
                throw new InvalidOperationException("Invalid when running using Automation");
            }

            //Find the plugin
            PluginRecord m_pr = Autodesk.Navisworks.Api.Application.Plugins.FindPlugin("ProvingGround.ConductorNavis.plugin.ProvingGround");
            if (m_pr != null && m_pr is DockPanePluginRecord && m_pr.IsEnabled)
            {

                // check if it needs loading
                if (m_pr.LoadedPlugin == null)
                {
                    string exeConfigPath = this.GetType().Assembly.Location;
                    m_pr.LoadPlugin();
                }

                DockPanePlugin m_dpp = m_pr.LoadedPlugin as DockPanePlugin;
                if (m_dpp != null)
                {

                    // switch the Visible flag
                    m_dpp.Visible = !m_dpp.Visible;

                }
            }
        }
    }
}
