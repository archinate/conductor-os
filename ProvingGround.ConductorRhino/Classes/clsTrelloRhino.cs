﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rhino;
using Rhino.DocObjects;
using Rhino.Geometry;

using TrelloNet;
using ProvingGround.ConductorInterface;
using ProvingGround.ConductorInterface.Classes;


namespace ProvingGround.ConductorRhino
{
    class clsTrelloRhino
    {

        public static void WriteArchive(List<RhinoObject> theseObjects, string filename)
        {

            // create the binary archive file
            Rhino.FileIO.BinaryArchiveFile m_file = new Rhino.FileIO.BinaryArchiveFile(
              filename,
              Rhino.FileIO.BinaryArchiveMode.Write);
            
            m_file.Open();

            m_file.Writer.WriteInt(theseObjects.Count);

            try
            {
                foreach (RhinoObject thisObject in theseObjects)
                {
                    // convert extrusions to breps
                    if (thisObject.Geometry is Rhino.Geometry.Extrusion)
                    {
                        Extrusion thisExtrusion = (Rhino.Geometry.Extrusion)thisObject.Geometry;
                        m_file.Writer.WriteGeometry((GeometryBase)thisExtrusion.ToBrep(true));
                    }
                    else
                    {
                        m_file.Writer.WriteGeometry(thisObject.Geometry);
                    }
                }
            }
            catch { }

            m_file.Close();

        }

        public static List<GeometryBase> ReadArchive()
        {

            var m_geometry = new List<GeometryBase>();

            Rhino.FileIO.BinaryArchiveFile m_file = new Rhino.FileIO.BinaryArchiveFile("C:\\Users\\David Stasiuk\\Desktop\\Temp\\test2.aect", Rhino.FileIO.BinaryArchiveMode.Read);
            m_file.Open();

            int objectCount = 0;

            try
            {
                objectCount = m_file.Reader.ReadInt();

                for (int i = 0; i < objectCount; i++)
                {
                    m_geometry.Add(m_file.Reader.ReadGeometry());
                }
            }
            catch { }

            m_file.Close();

            return m_geometry;

        }


    }
}
