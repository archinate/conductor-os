﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rhino;
using Rhino.DocObjects;
using Rhino.Geometry;

using TrelloNet;
using ProvingGround.ConductorInterface;
using ProvingGround.ConductorInterface.Classes;


namespace ProvingGround.ConductorRhino
{
    /// <summary>
    /// Interaction logic for form_TrelloPage.xaml
    /// </summary>
    public partial class form_TrelloPage : Page
    {
        private string _attachmentDir = "C:\\Proving Ground\\Trello\\ProvingGround.ConductorRhino\\Attachments\\";

        public form_TrelloPage()
        {
            clsCurrentApp.Handle = Rhino.RhinoApp.MainWindowHandle();
            InitializeComponent();
            //this.TrelloDock.CheckForCardObjects += e_CheckForCardObjects;
            //RhinoDoc.SelectObjects += e_ObjectsSelected;
        }

        /// <summary>
        /// Size Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetScaling();
        }

        /// <summary>
        /// Hot fix for windows 8 video DPI scaling
        /// </summary>
        private void SetScaling()
        {
            try
            {

                System.Windows.Media.Matrix m = PresentationSource.FromVisual(this).CompositionTarget.TransformToDevice;
                double dx = m.M11;
                double dy = m.M22;
                GridMain.Width = this.Width / dx;
                GridMain.Height = this.Height / dy;

            }
            catch { }
        }

        private void DoSetup()
        {
            //this.TrelloDock.TrelloUtils.
        }

        #region Form Operations

        private void Button_UpdateIDs_Click(object sender, RoutedEventArgs e)
        {
            var m_theseObjects = RhinoDoc.ActiveDoc.Objects.GetSelectedObjects(false, false).ToList();

            foreach (RhinoObject m_thisObject in m_theseObjects)
            {
                m_thisObject.Geometry.SetUserString(this.TrelloDock.SelectedCard.GetCardId(), "On Card");
                m_thisObject.CommitChanges();
            }
        }

        private void Button_AttachGeometry_Click(object sender, RoutedEventArgs e)
        {

            if (TrelloDock.IsCardSelected)
            {
                var m_selectedObjects = RhinoDoc.ActiveDoc.Objects.GetSelectedObjects(false, false).ToList();

                if (m_selectedObjects.Count > 0)
                {
                    Card m_card = TrelloDock.SelectedCard;

                    string m_filename = _attachmentDir + m_card.GetCardId() + ".trh";
                    
                    clsTrelloRhino.WriteArchive(m_selectedObjects, m_filename);
                    TrelloDock.TrelloUtils.AddAttachment(m_card, m_filename);
                }
                else
                {
                    MessageBox.Show("No rhino objects selected...");
                }
            }

        }

        #endregion

        #region Event-Driven Functions

        static void e_CheckForCardObjects(object sender, EventArgs e)
        {
            Card m_card = ((form_TrelloDock)sender).SelectedCard;
            string m_lookup = m_card.GetCardId();
            
            foreach (RhinoObject checkObject in RhinoDoc.ActiveDoc.Objects)
            {
                string userValue = checkObject.Geometry.GetUserString(m_lookup);
                if (userValue == "On Card")
                {
                    MessageBox.Show(userValue);
                    //checkObject.Highlight(true);
                    checkObject.Select(true, true);
                    checkObject.CommitChanges();
                }
            }

            RhinoDoc.ActiveDoc.Views.Redraw();
        }

        static void e_ObjectsSelected(object sender, RhinoObjectSelectionEventArgs e)
        {
            //MessageBox.Show(e.RhinoObjects.Length.ToString());
        }

        #endregion

        

        
    }
}


