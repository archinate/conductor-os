﻿using System;
using System.Collections.Generic;
using Rhino;
using Rhino.Commands;
using Rhino.Geometry;
using Rhino.Input;
using Rhino.Input.Custom;

namespace ProvingGround.ConductorRhino
{
    [System.Runtime.InteropServices.Guid("f2677639-b245-4362-b9b2-02a609368045")]
    public class CmdTrello : Command
    {
        /// <summary>
        /// Public constructor
        /// </summary>
        public CmdTrello()
        {
            Instance = this;
        }

        public static CmdTrello Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// The command name as it appears on the Rhino command line.
        /// </summary>
        public override string EnglishName
        {
            get { return "Conductor"; }
        }

        /// <summary>
        /// Called by Rhino when the user wants to run this command.
        /// </summary>
        protected override Result RunCommand(RhinoDoc doc, RunMode mode)
        {
                System.Guid panelId = TrelloPanelHost.PanelId;
                bool bVisible = Rhino.UI.Panels.IsPanelVisible(panelId);

                if (!bVisible)
                    Rhino.UI.Panels.OpenPanel(panelId);

                return Result.Success;

        }
    }
}
