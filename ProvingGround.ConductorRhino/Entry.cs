﻿using System.Reflection;
using System.ComponentModel;
using System.Windows;

using Rhino;
using Rhino.PlugIns;

namespace ProvingGround.ConductorRhino
{
    ///<summary>
    /// Every RhinoCommon plug-in must have one and only one Rhino.PlugIns.PlugIn
    /// inherited class. DO NOT create instances of this class yourself. It is the
    /// responsibility of Rhino to create an instance of this class.
    ///</summary>
    public class RhinoPlugin :PlugIn
    {
        public RhinoPlugin()
        {
            Instance = this;
        }

        ///<summary>Gets the only instance of the SampleCsWpfPanelPlugIn plug-in.</summary>
        public static RhinoPlugin Instance
        {
            get;
            private set;
        }

        // You can override methods here to change the plug-in behavior on
        // loading and shut down, add options pages to the Rhino _Option command
        // and mantain plug-in wide options in a document.

        /// <summary>
        /// Is called when the plug-in is being loaded.
        /// </summary>
        protected override Rhino.PlugIns.LoadReturnCode OnLoad(ref string errorMessage)
        {
            string app_name = Assembly.GetExecutingAssembly().GetName().Name;
            string app_version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            RhinoApp.WriteLine("{0} {1} loaded.", app_name, app_version);

            System.Type panel_type = typeof(TrelloPanelHost);
            Rhino.UI.Panels.RegisterPanel(this, panel_type, "Conductor", ProvingGround.ConductorRhino.Properties.Resources.PGSetIcon);

            return Rhino.PlugIns.LoadReturnCode.Success;
        }
    }

    /// <summary>
    /// Rhino framework requires a System.Windows.Forms.IWin32Window derived object for a panel.
    /// </summary>
    [System.Runtime.InteropServices.Guid("4f3383c0-bc16-4052-bce9-6655ed1c17a4")]
    public class TrelloPanelHost : RhinoWindows.Controls.WpfElementHost
    {
        public TrelloPanelHost()
            : base(new form_MainDock(), null) // No view model (for this example)
        {
        }

        /// <summary>
        /// Returns the ID of this panel.
        /// </summary>
        public static System.Guid PanelId
        {
            get
            {
                return typeof(TrelloPanelHost).GUID;
            }
        }
    }
}