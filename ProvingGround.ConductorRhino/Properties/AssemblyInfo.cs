﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Rhino.PlugIns;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Conductor for Rhino")]
[assembly: AssemblyDescription("Trello Task Manager for Rhino")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("PROVING GROUND")]
[assembly: AssemblyProduct("ProvingGround.ConductorRhino")]
[assembly: AssemblyCopyright("Copyright ©  2017 PROVING GROUND LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f2677639-b245-4362-b9b2-02a609368045")] // This will also be the Guid of the Rhino plug-in

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2017.1.19.0")]
[assembly: AssemblyFileVersion("2017.1.19.0")]
